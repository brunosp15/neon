package br.com.neontest.presenters;

import java.util.List;

import br.com.neontest.pojos.Client;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class SendMoneyPresenter extends BasePresenter {

    SendMoneyView mView;

    public SendMoneyPresenter(SendMoneyView view) {

        this.mView = view;
    }

    public void getClients() {
        List<Client> mockClients = Client.getMockClient();
        mView.setContacts(mockClients);
    }


    public interface SendMoneyView extends BaseView {
        void setContacts(List<Client> historic);
    }
}

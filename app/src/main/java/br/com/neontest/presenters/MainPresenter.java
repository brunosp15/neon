package br.com.neontest.presenters;

import android.content.Context;

import javax.inject.Inject;

import br.com.neontest.NeonApplication;
import br.com.neontest.communication.CustomCallback;
import br.com.neontest.communication.NeonService;
import br.com.neontest.utils.SessionManager;

/**
 * Created by bpinto2 on 11/26/16.
 */

public class MainPresenter {


    @Inject
    NeonService mService;

    @Inject
    Context mContext;

    public MainPresenter() {
        if (NeonApplication.getInstance() != null) {
            NeonApplication.getInstance().getComponent().inject(this);
        }
    }

    public void getToken() {
        mService.generateToken("Bruno da Silva Pinto", "brunosp18@gmail.com").enqueue(new CustomCallback<String>() {
            @Override
            public void onSuccess(String body) {
                saveToken(body);
            }

            @Override
            public void onError() {
                //Do nothing
            }
        });
    }

    private void saveToken(String token) {
        SessionManager.saveToken(mContext, token);
    }
}

package br.com.neontest.presenters;

import java.util.List;

import javax.inject.Inject;

import br.com.neontest.NeonApplication;
import br.com.neontest.communication.CustomCallback;
import br.com.neontest.communication.NeonService;
import br.com.neontest.pojos.Client;
import br.com.neontest.pojos.Transfer;
import br.com.neontest.utils.SessionManager;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class HistoricPresenter extends BasePresenter {

    HistoricView mView;

    @Inject
    NeonService mService;

    public HistoricPresenter(HistoricView mView) {
        if (NeonApplication.getInstance() != null) {
            NeonApplication.getInstance().getComponent().inject(this);
        }

        this.mView = mView;
    }

    public void getHistoric() {
        if (hasInternet()) {
            mView.showLoading(true);
            doHistRequest();
        } else {
            mView.showNoInternet();
        }
    }

    void doHistRequest() {
        mService.getTransfers(SessionManager.getToken(mContext)).enqueue(new CustomCallback<List<Transfer>>() {
            @Override
            public void onSuccess(List<Transfer> body) {
                onHistoricSuccess(body);
            }

            @Override
            public void onError() {
                onHistoricError();
            }
        });
    }

    void onHistoricError() {
        mView.showLoading(false);
        mView.showTryAgain();
    }

    void onHistoricSuccess(List<Transfer> transfers) {
        mView.showLoading(false);
        if (transfers.size() > 0) {
            joinClientData(transfers);
            mView.setHistoric(transfers);
        } else {
            mView.showEmptyView();
        }
    }

    void joinClientData(List<Transfer> transfers) {
        List<Client> mockClient = Client.getMockClient();

        for (int i = 0; i < transfers.size(); i++) {
            Transfer transfer = transfers.get(i);
            transfer.setClient(mockClient.get(transfer.getClientId() - 1));
        }
    }

    public interface HistoricView extends BaseView {
        void setHistoric(List<Transfer> historic);

        void showEmptyView();

        void showTryAgain();

        void showLoading(boolean show);

    }
}

package br.com.neontest.presenters;

import android.support.annotation.NonNull;

import java.util.Date;

import javax.inject.Inject;

import br.com.neontest.NeonApplication;
import br.com.neontest.R;
import br.com.neontest.communication.CustomCallback;
import br.com.neontest.communication.NeonService;
import br.com.neontest.pojos.Transfer;
import br.com.neontest.utils.SessionManager;

/**
 * Created by bpinto2 on 11/27/16.
 */

public class DialogPresenter extends BasePresenter {

    private static final double MAX_LIMIT = 10000;

    private DialogView mView;

    @Inject
    NeonService mService;


    public DialogPresenter(DialogView mView) {
        if (NeonApplication.getInstance() != null) {
            NeonApplication.getInstance().getComponent().inject(this);
        }
        this.mView = mView;
    }

    public void sendMoney(int clientId, double amount) {
        if (amount <= MAX_LIMIT) {
            if (hasInternet()) {
                Transfer send = createTransfer(clientId, amount);
                doSendRequest(send);
            } else {
                mView.showNoInternet();
            }
        } else {
            mView.onSendError(R.string.send_max_error);
        }
    }

    @NonNull
    Transfer createTransfer(int clientId, double amount) {
        return new Transfer(SessionManager.getToken(mContext), clientId, amount, new Date());
    }

    void doSendRequest(Transfer send) {
        mService.sendMoney(send).enqueue(new CustomCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean body) {
                if (body) {
                    mView.onSendSuccess();
                } else {
                    mView.onSendError(R.string.send_generic_error);
                }
            }

            @Override
            public void onError() {
                mView.onSendError(R.string.send_generic_error);
            }
        });
    }

    public interface DialogView extends BasePresenter.BaseView {

        void onSendSuccess();

        void onSendError(int errorId);
    }
}

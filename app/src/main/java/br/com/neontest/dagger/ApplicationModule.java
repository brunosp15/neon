package br.com.neontest.dagger;

import android.content.Context;

import javax.inject.Singleton;

import br.com.neontest.NeonApplication;
import dagger.Module;
import dagger.Provides;

/**
 * Created by bpinto2 on 11/16/16.
 */
@Module
public class ApplicationModule {

    private NeonApplication mApplication;

    public ApplicationModule(NeonApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }
}


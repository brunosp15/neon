package br.com.neontest.dagger;

import javax.inject.Singleton;

import br.com.neontest.presenters.DialogPresenter;
import br.com.neontest.presenters.HistoricPresenter;
import br.com.neontest.presenters.MainPresenter;
import dagger.Component;

/**
 * Created by bpinto2 on 11/16/16.
 */
@Singleton
@Component(modules = {
        RestClientModule.class,
        ApplicationModule.class
})
public interface ApplicationComponent {


    void inject(MainPresenter mainPresenter);

    void inject(HistoricPresenter historicPresenter);

    void inject(DialogPresenter dialogPresenter);
}

package br.com.neontest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.neontest.R;
import br.com.neontest.pojos.Client;
import br.com.neontest.utils.CircleTransform;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class PhotoImageView extends FrameLayout {

    @BindView(R.id.view_initial)
    TextView initials;

    @BindView(R.id.view_photo)
    ImageView photo;

    public PhotoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PhotoImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_photo, this);
        ButterKnife.bind(this);
    }

    public void setClient(Client client) {
        Glide.with(getContext())
                .load(client.getPhoto())
                .transform(new CircleTransform(getContext()))
                .into(photo);

        initials.setText(client.getInitials());
    }
}

package br.com.neontest.communication;

import java.util.List;

import br.com.neontest.pojos.Transfer;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by bpinto2 on 11/16/16.
 */

public interface NeonService {


    @GET("/generateToken")
    Call<String> generateToken(@Query("nome") String name, @Query("email") String email);

    @POST("/sendMoney")
    Call<Boolean> sendMoney(@Body Transfer body);

    @GET("getTransfers")
    Call<List<Transfer>> getTransfers(@Query("token") String token);

}

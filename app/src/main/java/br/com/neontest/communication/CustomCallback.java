package br.com.neontest.communication;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bpinto2 on 11/16/16.
 */

public abstract class CustomCallback<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            onSuccess(response.body());
        } else {
            onError();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onError();
    }

    public abstract void onSuccess(T body);

    public abstract void onError();
}

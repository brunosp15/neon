package br.com.neontest.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by bpinto2 on 11/26/16.
 */

public class Transfer {

    private int id;
    private String token;

    @SerializedName("ClienteId")
    private int clientId;

    @SerializedName("Valor")
    private double amount;

    private Client client;

    public Transfer(String token, int clientId, double amount, Date date) {
        this.token = token;
        this.clientId = clientId;
        this.amount = amount;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}

package br.com.neontest.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class Client implements Serializable {

    private int id;
    private String name;
    private String phone;
    private String photo;

    public Client(int id, String name, String phone, String photo) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPhoto() {
        return photo;
    }

    public String getInitials() {
        String[] split = name.split(" ");
        if (split.length >= 2) {
            return String.valueOf(split[0].substring(0, 1) + split[1].substring(0, 1));
        } else if (split.length > 0) {
            return String.valueOf(split[0].substring(0, 1));
        } else {
            return "";
        }
    }

    public static List<Client> getMockClient() {
        ArrayList<Client> clients = new ArrayList<>(15);
        clients.add(new Client(1, "Aeronauta Barata", "(11) 3224-4000", "https://puu.sh/sukQI/2b75dc7e40.png"));
        clients.add(new Client(2, "Agrícola Beterraba Areia", "(11) 3224-4000", "https://puu.sh/sukQr/1a5bc1a33a.png"));
        clients.add(new Client(3, "Agrícola da Terra Fonseca ", "(11) 3224-4000", "https://puu.sh/sukPm/cf5d21be2c.png"));
        clients.add(new Client(4, "Alce Barbuda", "(11) 3224-4000", "https://puu.sh/sukPa/c0ef716b6a.png"));
        clients.add(new Client(5, "Amado Amoroso ", "(11) 3224-4000", "https://puu.sh/sukOL/d399783c3d.png"));
        clients.add(new Client(6, "Amável Pinto", "(11) 3224-4000", "https://puu.sh/sukOk/c8deda604c.png"));
        clients.add(new Client(7, "Amazonas Rio do Brasil Pimpão ", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(8, "América do Sul Brasil de Santana", "(11) 3224-4000", "https://puu.sh/sukNU/8c4d5dc5f8.png"));
        clients.add(new Client(9, "Amin Amou Amado ", "(11) 3224-4000", "https://puu.sh/sukNJ/cad5d5239f.png"));
        clients.add(new Client(10, "Antonio Manso Pacífico de Oliveira Sossegado", "(11) 3224-4000", "https://puu.sh/sukNs/32d0de2b1d.png"));
        clients.add(new Client(11, "Antônio Morrendo das Dores", "(11) 3224-4000", "https://puu.sh/sukMt/374e6bbdb8.png"));
        clients.add(new Client(12, "Asteróide Silverio", "(11) 3224-4000", "https://puu.sh/sukM7/819a6e2cb5.png"));
        clients.add(new Client(13, "Bandeirante do Brasil Paulistano", "(11) 3224-4000", "https://puu.sh/sukLU/27690fa793.png"));
        clients.add(new Client(14, "Céu Azul do Sol Poente", "(11) 3224-4000", null));
        clients.add(new Client(15, "Barrigudinha Seleida", "(11) 3224-4000", null));
        clients.add(new Client(16, "Bispo de Paris", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(17, "Chevrolet da Silva Ford ", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(18, "Dezêncio Feverêncio de Oitenta e Cinco", "(11) 3224-4000", null));
        clients.add(new Client(19, "Dolores Fuertes de Barriga", "(11) 3224-4000", null));
        clients.add(new Client(20, "Esparadrapo Clemente de Sá", "(11) 3224-4000", null));
        clients.add(new Client(21, "Homem Bom da Cunha Souto Maior", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(22, "Ilegível Inilegível ", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(23, "Inocêncio Coitadinho", "(11) 3224-4000", null));
        clients.add(new Client(24, "Janeiro Fevereiro de Março Abril", "(11) 3224-4000", null));
        clients.add(new Client(25, "Lança Perfume Rodometálico de Andrade ", "(11) 3224-4000", null));
        clients.add(new Client(26, "Marciano Verdinho das Antenas Longas", "(11) 3224-4000", "https://puu.sh/sukO7/2833f5f7a7.png"));
        clients.add(new Client(27, "Maria Privada de Jesus", "(11) 3224-4000", null));
        clients.add(new Client(28, "Maria Tributina Prostituta Cataerva ", "(11) 3224-4000", null));
        clients.add(new Client(29, "Maria-você-me-mata", "(11) 3224-4000", null));
        clients.add(new Client(30, "Mimaré Índio Brazileiro de Campos", "(11) 3224-4000", null));

        return clients;

    }

}


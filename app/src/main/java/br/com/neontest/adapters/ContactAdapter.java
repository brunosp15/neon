package br.com.neontest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.neontest.R;
import br.com.neontest.pojos.Client;
import br.com.neontest.pojos.Transfer;
import br.com.neontest.views.PhotoImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class ContactAdapter<T> extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {


    private final boolean mShowValue;
    private final OnContactClickListener mCallback;
    private List<T> mList;

    public ContactAdapter(List<T> list, OnContactClickListener listener, boolean showValue) {
        mShowValue = showValue;
        mList = list;
        mCallback = listener;
    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ContactViewHolder(view, mCallback, mShowValue);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        T obj = mList.get(position);
        if (obj instanceof Client) {
            holder.bind((Client) obj);
        } else if (obj instanceof Transfer) {
            holder.bind((Transfer) obj);
        } else {
            throw new IllegalArgumentException("List must be of Client or Transfer");
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        private final boolean showValue;
        private final OnContactClickListener callback;

        @BindView(R.id.contact_name)
        TextView name;

        @BindView(R.id.contact_phone)
        TextView phone;

        @BindView(R.id.contact_photo)
        PhotoImageView photo;

        @BindView(R.id.contact_value)
        TextView value;

        public ContactViewHolder(View itemView, final OnContactClickListener callback, boolean showValue) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.showValue = showValue;
            this.callback = callback;

        }

        public void bind(final Transfer transfer) {
            final Client client = transfer.getClient();
            bind(client);
            value.setText(String.format("%1$,.2f", transfer.getAmount()));
        }

        public void bind(final Client client) {
            name.setText(client.getName());
            phone.setText(client.getPhone());
            photo.setClient(client);
            value.setVisibility(showValue ? View.VISIBLE : View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onContactClick(client, photo);
                }
            });
        }
    }

    public interface OnContactClickListener {
        void onContactClick(Client client, PhotoImageView photo);
    }
}

package br.com.neontest.adapters;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.neontest.R;
import br.com.neontest.pojos.Transfer;
import br.com.neontest.views.PhotoImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    private final double ratio;

    List<Transfer> mList;

    public TimeLineAdapter(List<Transfer> mList, Context context) {
        this.mList = mList;
        Collections.sort(mList, new Comparator<Transfer>() {
            @Override
            public int compare(Transfer transfer2, Transfer transfer1) {
                if (transfer1.getAmount() > transfer2.getAmount()) {
                    return 1;
                } else if (transfer1.getAmount() < transfer2.getAmount()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        ratio = context.getResources().getDimension(R.dimen.hist_line) / mList.get(0).getAmount();

    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hist, parent, false);
        return new TimeLineViewHolder(view, ratio);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {
        Transfer transfer = mList.get(position);
        holder.bind(transfer);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class TimeLineViewHolder extends RecyclerView.ViewHolder {

        private final double ratio;
        @BindView(R.id.hist_line)
        View line;

        @BindView(R.id.hist_photo)
        PhotoImageView photo;

        @BindView(R.id.hist_value)
        TextView value;
        private boolean alreadyAnimated;

        public TimeLineViewHolder(View itemView, double ratio) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.ratio = ratio;
        }

        public void bind(Transfer transfer) {
            value.setText(String.format("%1$,.2f", transfer.getAmount()));
            photo.setClient(transfer.getClient());
            int height = (int) ((ratio * transfer.getAmount()));
            if (alreadyAnimated) {
                line.getLayoutParams().height = (int) ((ratio * transfer.getAmount()));
            } else {
                animate(height);
            }
        }

        private void animate(int height) {
            alreadyAnimated = true;
            ValueAnimator slideAnimator = ValueAnimator
                    .ofInt(0, height)
                    .setDuration(300);

            slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer value = (Integer) animation.getAnimatedValue();
                    line.getLayoutParams().height = value.intValue();
                    line.requestLayout();
                }
            });

            AnimatorSet set = new AnimatorSet();
            set.play(slideAnimator);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
        }
    }
}

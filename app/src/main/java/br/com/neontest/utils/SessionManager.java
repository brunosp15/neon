package br.com.neontest.utils;

import android.content.Context;

/**
 * Created by bpinto2 on 11/26/16.
 */

public class SessionManager {

    private static final String SESSION = "session";
    private static final String TOKEN = "token";

    public static void saveToken(Context context, String token) {
        context.getSharedPreferences(SESSION, Context.MODE_PRIVATE).edit().putString(TOKEN, token).apply();
    }

    public static String getToken(Context context) {
        return context.getSharedPreferences(SESSION, Context.MODE_PRIVATE).getString(TOKEN, null);
    }
}

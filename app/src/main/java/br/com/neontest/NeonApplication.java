package br.com.neontest;

import android.app.Application;

import br.com.neontest.dagger.ApplicationComponent;
import br.com.neontest.dagger.ApplicationModule;
import br.com.neontest.dagger.DaggerApplicationComponent;


/**
 * Created by bpinto2 on 11/26/16.
 */

public class NeonApplication extends Application {

    private static NeonApplication sInstance;
    private ApplicationComponent mComponent;

    public static synchronized NeonApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        setupModules();
    }

    private void setupModules() {
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }
}

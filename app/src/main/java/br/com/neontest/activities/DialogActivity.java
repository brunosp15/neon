package br.com.neontest.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import br.com.neontest.R;
import br.com.neontest.pojos.Client;
import br.com.neontest.presenters.DialogPresenter;
import br.com.neontest.views.PhotoImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bpinto2 on 11/27/16.
 */

public class DialogActivity extends BaseActivity implements DialogPresenter.DialogView {

    @BindView(R.id.dialog_name)
    TextView mName;

    @BindView(R.id.dialog_phone)
    TextView mPhone;

    @BindView(R.id.dialog_progress)
    View mProgress;

    @BindView(R.id.dialog_photo)
    PhotoImageView mPhoto;

    @BindView(R.id.dialog_btn)
    View mButton;

    @BindView(R.id.dialog_amount)
    EditText mAmount;

    @BindView(R.id.dialog_error)
    TextView mError;

    private int mClientId;

    private DialogPresenter mPresenter;

    private static final String CLIENT = "client";

    public static Intent newIntent(Context context, Client client) {
        Intent intent = new Intent(context, DialogActivity.class);
        intent.putExtra(CLIENT, client);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_send);
        ButterKnife.bind(this);
        Client client = (Client) getIntent().getSerializableExtra(CLIENT);
        setClient(client);
        mPresenter = new DialogPresenter(this);
    }

    public void setClient(Client client) {
        mClientId = client.getId();
        mName.setText(client.getName());
        mPhone.setText(client.getPhone());
        mPhoto.setClient(client);
    }

    @OnClick(R.id.dialog_btn)
    public void onBtnClick() {
        showLoading(true);
        mPresenter.sendMoney(mClientId, Double.parseDouble(mAmount.getText().toString()));
    }

    @OnClick(R.id.dialog_close)
    public void onCloseClick() {
        setResult(Activity.RESULT_CANCELED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    public void showLoading(boolean show) {
        if (show) {
            mProgress.setVisibility(View.VISIBLE);
            mButton.setVisibility(View.GONE);
        } else {
            mProgress.setVisibility(View.GONE);
            mButton.setVisibility(View.VISIBLE);
        }
    }


    public void showError(@StringRes int error) {
        if (error > 0) {
            mError.setText(error);
        } else {
            mError.setText(null);
        }
    }

    @Override
    public void onSendSuccess() {
        showLoading(false);
        setResult(Activity.RESULT_OK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    @Override
    public void onSendError(int errorId) {
        showLoading(false);
        showError(errorId);
    }

    @Override
    public void showNoInternet() {
        showLoading(false);
        showError(R.string.no_internet_connection);
    }
}

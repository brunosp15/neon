package br.com.neontest.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

import br.com.neontest.R;
import br.com.neontest.adapters.ContactAdapter;
import br.com.neontest.adapters.TimeLineAdapter;
import br.com.neontest.pojos.Client;
import br.com.neontest.pojos.Transfer;
import br.com.neontest.presenters.HistoricPresenter;
import br.com.neontest.views.PhotoImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class HistoricActivity extends BaseActivity implements HistoricPresenter.HistoricView, ContactAdapter.OnContactClickListener {


    @BindView(R.id.hist_time_line)
    RecyclerView mTimeLine;

    @BindView(R.id.hist_list)
    RecyclerView mList;

    @BindView(R.id.hist_empty)
    View mEmpty;

    @BindView(R.id.hist_loading)
    View mLoading;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private HistoricPresenter mPresenter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, HistoricActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Histórico de Envios");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mPresenter = new HistoricPresenter(this);
        mTimeLine.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mList.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.getHistoric();
    }

    @Override
    public void setHistoric(final List<Transfer> historic) {
        mTimeLine.setAdapter(new TimeLineAdapter(historic, this));
        mList.setAdapter(new ContactAdapter(historic, this, true));
    }

    @Override
    public void showEmptyView() {
        mEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTryAgain() {
        Snackbar.make(mToolbar, "Algo deu errado", Snackbar.LENGTH_INDEFINITE)
                .setAction("Tentar novamente", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.getHistoric();
                    }
                }).show();
    }

    @Override
    public void showLoading(boolean show) {
        mLoading.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onContactClick(Client client, PhotoImageView photo) {
        //Do Nothing
    }

    @Override
    public void showNoInternet() {
        Snackbar.make(mToolbar, "Conecte-se a internet e tente novamente", Snackbar.LENGTH_INDEFINITE)
                .setAction("Tentar Novamente", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.getHistoric();
                    }
                }).show();
    }
}

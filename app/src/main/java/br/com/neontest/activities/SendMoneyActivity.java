package br.com.neontest.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.List;

import br.com.neontest.R;
import br.com.neontest.adapters.ContactAdapter;
import br.com.neontest.pojos.Client;
import br.com.neontest.presenters.SendMoneyPresenter;
import br.com.neontest.views.PhotoImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class SendMoneyActivity extends BaseActivity implements SendMoneyPresenter.SendMoneyView, ContactAdapter.OnContactClickListener {

    private static final int SEND_REQUEST = 1;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.send_list)
    RecyclerView mList;

    private SendMoneyPresenter mPresenter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SendMoneyActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Enviar Dinheiro");
        mList.setLayoutManager(new LinearLayoutManager(this));
        mPresenter = new SendMoneyPresenter(this);
        mPresenter.getClients();
    }

    @Override
    public void setContacts(List<Client> historic) {
        mList.setAdapter(new ContactAdapter(historic, this, false));
    }

    @Override
    public void onContactClick(final Client client, PhotoImageView photo) {
        Intent intent = DialogActivity.newIntent(this, client);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, photo, "client_photo");
            startActivityForResult(intent, SEND_REQUEST, options.toBundle());
        } else {
            startActivityForResult(intent, SEND_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEND_REQUEST && resultCode == Activity.RESULT_OK) {
            Snackbar.make(mList, "Transferência efetuada!", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showNoInternet() {
        //Do nothing
    }
}

package br.com.neontest.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.neontest.R;
import br.com.neontest.presenters.MainPresenter;
import br.com.neontest.utils.CircleTransform;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bpinto2 on 11/25/16.
 */

public class MainActivity extends BaseActivity {


    @BindView(R.id.main_photo)
    ImageView mPhoto;

    private MainPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Glide.with(this)
                .load("https://puu.sh/suzqc/51a9b83f43.png")
                .transform(new CircleTransform(this))
                .placeholder(R.drawable.user_circle_sample)
                .into(mPhoto);

        mPresenter = new MainPresenter();
        mPresenter.getToken();
    }

    @OnClick(R.id.main_send_btn)
    public void onSendClick() {
        startActivity(SendMoneyActivity.newIntent(this));
    }

    @OnClick(R.id.main_hist_btn)
    public void onHistClick() {
        startActivity(HistoricActivity.newIntent(this));
    }
}

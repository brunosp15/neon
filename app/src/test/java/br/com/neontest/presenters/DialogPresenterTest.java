package br.com.neontest.presenters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Date;

import br.com.neontest.pojos.Transfer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import static org.mockito.Mockito.spy;

/**
 * Created by bpinto2 on 11/27/16.
 */

public class DialogPresenterTest {

    DialogPresenter mPresenter;

    @Mock
    DialogPresenter.DialogView mView;

    @Before
    public void setup() {
        initMocks(this);
        mPresenter = spy(new DialogPresenter(mView));
        doNothing().when(mPresenter).doSendRequest(any(Transfer.class));
    }

    @Test
    public void when_sendMoney_above_the_limit_should_show_error() {
        mPresenter.sendMoney(10, 11000);

        verify(mView).onSendError(anyInt());
    }

    @Test
    public void when_sendMoney_bellow_the_limit_and_without_internet_should_show_no_internet() {
        doReturn(false).when(mPresenter).hasInternet();

        mPresenter.sendMoney(10, 999);

        verify(mView).showNoInternet();
    }

    @Test
    public void when_sendMoney_bellow_the_limit_and_with_internet_should_do_the_request() {
        doReturn(true).when(mPresenter).hasInternet();
        doReturn(new Transfer("", 1, 100, new Date())).when(mPresenter).createTransfer(anyInt(), anyDouble());

        mPresenter.sendMoney(10, 999);

        verify(mPresenter).doSendRequest(any(Transfer.class));
    }


}

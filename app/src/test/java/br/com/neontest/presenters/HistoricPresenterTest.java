package br.com.neontest.presenters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.neontest.pojos.Transfer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by bpinto2 on 11/27/16.
 */

public class HistoricPresenterTest {

    @Mock
    HistoricPresenter.HistoricView mView;

    private HistoricPresenter mPresenter;

    @Before
    public void setup() {
        initMocks(this);
        mPresenter = spy(new HistoricPresenter(mView));
        doNothing().when(mPresenter).doHistRequest();
    }

    @Test
    public void when_getHistoric_should_show_loading_and_do_reqeust() {
        doReturn(true).when(mPresenter).hasInternet();

        mPresenter.getHistoric();

        verify(mView).showLoading(true);
        verify(mPresenter).doHistRequest();
    }

    @Test
    public void when_getHistoric_without_internet_should_show_no_internet() {
        doReturn(false).when(mPresenter).hasInternet();

        mPresenter.getHistoric();

        verify(mView).showNoInternet();
    }

    @Test
    public void when_onHistoricError_should_hide_loading_aind_show_try_again() {
        mPresenter.onHistoricError();

        verify(mView).showLoading(false);
        verify(mView).showTryAgain();
    }

    @Test
    public void when_onHistoricSuccess_should_hide_loading() {
        ArrayList<Transfer> transfers = new ArrayList<>();
        transfers.add(new Transfer("", 1, 1, new Date()));

        mPresenter.onHistoricSuccess(transfers);

        verify(mView).showLoading(false);

    }

    @Test
    public void when_onHistoricSuccess_should_with_size_should_join_and_setHistoric() {
        ArrayList<Transfer> transfers = new ArrayList<>();
        transfers.add(new Transfer("", 1, 1, new Date()));

        mPresenter.onHistoricSuccess(transfers);

        verify(mPresenter).joinClientData(transfers);
        verify(mView).setHistoric(transfers);
    }

    @Test
    public void when_onHistoricSuccess_should_with_empty_should_show_empty() {
        ArrayList<Transfer> transfers = new ArrayList<>();

        mPresenter.onHistoricSuccess(transfers);

        verify(mView).showEmptyView();
    }

}

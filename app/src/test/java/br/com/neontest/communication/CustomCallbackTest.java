package br.com.neontest.communication;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class CustomCallbackTest {

    CustomCallback mCustomCallback;

    @Before
    public void setup() {
        mCustomCallback = spy(new CustomCallback() {
            @Override
            public void onSuccess(Object body) {

            }

            @Override
            public void onError() {

            }
        });
    }

    @Test
    public void when_onResponse_with_successful_should_call_onSuccess() {
        mCustomCallback.onResponse(null, Response.success(new Object()));

        verify(mCustomCallback, times(1)).onSuccess(Matchers.anyObject());
    }

    @Test
    public void when_onResponse_with_NOT_successful_should_call_onError() {
        mCustomCallback.onResponse(null, Response.error(400, ResponseBody.create(MediaType.parse("json"), "")));

        verify(mCustomCallback, times(1)).onError();
    }


    @Test
    public void when_onFailure_should_call_onError() {
        mCustomCallback.onFailure(any(Call.class), any(Throwable.class));

        verify(mCustomCallback, times(1)).onError();
    }
}
